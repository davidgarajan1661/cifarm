const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarios-controller');

router.get('/', usuarioController.getUsuarios);
router.get('/:id_usuario', usuarioController.getUsuario);
router.post('/cadastro', usuarioController.postUsuario);

module.exports = router;